<?php

class Login_m extends CI_Model {

	public function __construct()
	{
		//$dsn = 'mysql://pictrapA:Apartcip@localhost/pictrackdb';
		//$this->load->database($dsn);
		$this->load->database();
		$this->load->dbforge();
	}
	
	public function log_user_in($uniqid, $logintype)
	{
		$accountID = $this->check_if_account_exists($uniqid, $logintype);
		if(FALSE === $accountID)
		{	$accountID = $this->register_new_user($uniqid, $logintype);	}
		else
		{	$this->increment_numlogins($accountID);	}
		
		return $accountID;
	}
	
	private function check_if_account_exists($uniqid, $logintype)
	{
		$result = $this->db->get_where('account', array('loginIdentifier' => $uniqid, 'logintype' => $logintype), 1, 0)->result_array();
		
		
		if(count($result) == 1)
		{	return $result[0]['accountID'];	}
		else
		{	return FALSE;	}
	}
	
	private function increment_numlogins($accountID)
	{
		$this->db->where('accountID', $accountID);
		$this->db->set('numlogins', 'numlogins+1', FALSE);
		$this->db->update('account');
	}
	
	private function register_new_user($uniqid, $logintype)
	{
		$accountID = $this->gen_accountID();
		
		$newuser = array(
							'accountID' => $accountID,
							'logintype' => $logintype,
							'loginIdentifier' => $uniqid,
							'numlogins' => 1
						);
		//required so CI doesnt escape NOW() call
		$this->db->set('lastlogin', 'NOW()', FALSE);
		$this->db->set('signuptime', 'NOW()', FALSE);
		
		$result = $this->db->insert('account', $newuser); 
		if($result)
		{	
			$this->build_out_table($accountID);
			return $accountID;
		}
		else
		{	return FALSE;	}
	}
	
	private function gen_accountID()
	{
		$characters = '23456789ABCDEFGHJKMNPQRSTVWXYZ';
		$accountIDs = $this->get_all_accountIDs();
					
		do
		{	
			$tmpid = '';
			for($counter = 0; $counter < 6; $counter++)
			{	
				$position = mt_rand(0, 29);
				$tmpid .= $characters[$position];
			}
		}				
		while(in_array($tmpid, $accountIDs)); // while in database
		
		return $tmpid;
	}
	
	private function get_all_accountIDs()
	{
		$this->db->select('accountID');	
		$query = $this->db->get('account');
		$rows = $query->result();
		
		$ids = array();
		foreach ($rows as $r)
			$ids[] = $r->accountID; 
		
		return $ids;
	}
	
	private function build_out_table($accountID)
	{
		$fields = array(
							'devicetime' => array('type' => 'BIGINT', 'primarykey' => TRUE),
							'public' => array('type' => 'TINYINT', 'default' => 0),
							'lat' => array('type' => 'FLOAT', 'default' => 0.0),
							'lon' => array('type' => 'FLOAT', 'default' => 0.0),
							'comment' => array('type' => 'TINYTEXT'),
							'delete' => array('type' => 'TINYINT', 'default' => 0)
						);
        $this->dbforge->add_key('devicetime', TRUE);
        $this->dbforge->add_field($fields);		
		$this->dbforge->create_table('pic_'.$accountID);
	}	
}
