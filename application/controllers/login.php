<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller 
{
	public function index()
	{	
		$this->load->helper(array('form', 'url'));		
		$this->load->view('login/form');
	}
	
	public function signup()
	{
		//$this->load->view('login/form');
		echo 'signup form';
	}
	
	public function openid()
	{
		$this->load->helper(array('form', 'url'));
				
		$this->load->library('form_validation');
		$this->form_validation->set_rules('openid_identifier', 'Openid Url', 'required');
		
		if($this->form_validation->run() === FALSE)
		{
			
			$this->load->view('login/form');
		}
		else
		{
			try
			{
				require FCPATH.APPPATH.'third_party/lightopenid/openid.php';
				$openid = new LightOpenID(base_url());
				if(!$openid->mode)
				{
					$openid->identity = $_POST['openid_identifier'];

					$openid->required = array('namePerson/first', 'namePerson/last', 'contact/email');
					
					header('Location: '.$openid->authUrl());
				}
				elseif($openid->mode == 'cancel')
				{
					//cancled login
					$data['msg'] = 'canceled login';
					$this->load->view('login/failed', $data);
				}
				elseif($openid->validate())
				{
					//logged in
					//$data['identity'] = $openid->identity;
					$this->_log_user_in($openid->identity, 'OID');
					//$this->load->view('login/success', $data);
				}
				else
				{
					//log in failed
					$data['msg'] = 'login failed';
					$this->load->view('login/failed', $data);
				}
			}
			catch(ErrorException $e)
			{
				$data['msg'] = 'Error: '.$e->getMessage();
				$this->load->view('login/failed', $data);
			}
		}
	}
	
	function google()
	{
		$this->load->helper(array('url'));
		require FCPATH.APPPATH.'third_party/lightopenid/openid.php';
		$openid = new LightOpenID(base_url());
		
		try
		{
			if(!$openid->mode) 
			{
				$openid->identity = 'https://www.google.com/accounts/o8/id';
				header('Location: '.$openid->authUrl());
			} 
			elseif($openid->mode == 'cancel') 
			{
				$data['msg'] = 'canceled login';
				$this->load->view('login/failed', $data);
			}
			elseif($openid->validate())
			{
				//logged in
				//$data['identity'] = $openid->identity;
				$this->_log_user_in($openid->identity, 'OID');
				//$this->load->view('login/success', $data);
			}
			else
			{
				//log in failed
				$data['msg'] = 'login failed';
				$this->load->view('login/failed', $data);
			}
		} 
		catch(ErrorException $e)
		{
			$data['msg'] = 'Error: '.$e->getMessage();
			$this->load->view('login/failed', $data);
		}
	}
	
	function facebook()
	{
		$this->load->helper(array('url'));
		require FCPATH.APPPATH.'third_party/facebook/facebook.php';
		
		$facebook = new Facebook(array('appId'  => '298835083548232', 'secret' => '2bfed173253f8a00f979b4ba37cdd242',));
		$user = $facebook->getUser();
		
		if($user)
		{
			try 
			{// Proceed knowing you have a logged in user who's authenticated.
				$user_profile = $facebook->api('/me');
				//$data['identity'] = $user_profile['id'];
				$this->_log_user_in($user_profile['id'], 'FB');
				//$this->load->view('login/success', $data);
			} 
			catch(FacebookApiException $e)
			{
				$data['msg'] = 'Error: '.$e->getMessage();
				$this->load->view('login/failed', $data);
			}			
		}
		else
		{
			header('Location: '.$facebook->getLoginUrl());
		}
	}
	
	private function _log_user_in($uniqid, $logintype)
	{
		$this->load->model('login_m');
		$result = $this->login_m->log_user_in($uniqid, $logintype);
		
		if(FALSE === $result)
		{
			$data['msg'] = 'error occured logging you in to pictrap';
			$this->load->view('login/failed', $data);
		}
		else
		{	//TODO: START SESSION HERE
			//session auto loaded
			$this->session->set_userdata(array('accountID' => $result));
			
			$data['identity'] = $result;
			$this->load->view('login/success', $data);
		}
	}
}
