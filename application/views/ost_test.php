<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en">
<head>
  <title>CloudMade JavaScript API test</title>
</head>
<body>

  <div id="cm-example" style="width: 1000px; height: 700px;"></div>

  <script type="text/javascript" src="http://tile.cloudmade.com/wml/latest/web-maps-lite.js"></script>
  <script type="text/javascript">
    var cloudmade = new CM.Tiles.CloudMade.Web({key: '8ee2a50541944fb9bcedded5165f09d9'});
    var map = new CM.Map('cm-example', cloudmade);
    map.setCenter(new CM.LatLng(53.346862,-6.275253), 12);
    
    var topRight = new CM.ControlPosition(CM.TOP_RIGHT, new CM.Size(0, 0));
    
    map.addControl(new CM.OverviewMapControl(), topRight);
    map.addControl(new CM.ScaleControl());
    map.addControl(new CM.LargeMapControl());
    
    var MyCustomControl = function() {};
    
	MyCustomControl.prototype = {
		initialize: function(map, position) {
			var control = document.createElement('div');
			
			control.style.background = 'white';
			control.style.padding = '5px';
			control.style.border = '1px solid #ccc';
			
			function createMapLink(location, zoom, name) {
				var link = document.createElement('a');
				link.href = '';
				link.innerHTML = "Go to " + name;
				link.onclick = function() {
					map.setCenter(location, zoom);
					return false;
				}
				control.appendChild(link);
				control.appendChild(document.createElement('br'));
			}
			
			createMapLink(new CM.LatLng(51.5084, -0.1255), 11, 'London');
			createMapLink(new CM.LatLng(52.516, 13.398), 11, 'Berlin');
			createMapLink(new CM.LatLng(37.7533,-122.4293), 12, 'San Francisco');
			createMapLink(new CM.LatLng(0,0), 1, 'World View');
			
			map.getContainer().appendChild(control);
			return control;
		},
		
		getDefaultPosition: function() {
			return new CM.ControlPosition(CM.BOTTOM_RIGHT, new CM.Size(0, 15));
		}
	}
	map.addControl(new MyCustomControl());
	
	var trinLatLng = new CM.LatLng(53.344582,-6.259439);
	var trinIcon = new CM.Icon();
	trinIcon.image  = "<?= $this->config->base_url(); ?>images/trinity.png";
	trinIcon.iconSize = new CM.Size(45, 62);
	var trinMarker = new CM.Marker(trinLatLng, {title: "Trinity", icon: trinIcon, draggable: true});
	map.addOverlay(trinMarker);
	
	CM.Event.addListener(trinMarker, 'dragend', function() {
		console.log(trinMarker.getLatLng().lat() + ', ' + trinMarker.getLatLng().lat());
	});
	
	var gecMarker = new CM.Marker(new CM.LatLng(53.34111,-6.283944), {title: "Work", draggable: true, icon: new CM.Icon({image: "<?= $this->config->base_url(); ?>images/gec.png", iconSize: new CM.Size(50, 50)})});
	map.addOverlay(gecMarker);
		
	CM.Event.addListener(gecMarker, 'dragend', function() {
		console.log(gecMarker.getLatLng().lat() + ', ' + gecMarker.getLatLng().lat());
	});
		
	var clusterer = new CM.MarkerClusterer(map, {clusterRadius: 70});
	clusterer.addMarkers([trinMarker, gecMarker]);
  </script>

</body>
</html>
